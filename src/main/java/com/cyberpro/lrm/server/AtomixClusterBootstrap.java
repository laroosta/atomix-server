package com.cyberpro.lrm.server;

import io.atomix.cluster.Node;
import io.atomix.cluster.discovery.BootstrapDiscoveryProvider;
import io.atomix.core.Atomix;
import io.atomix.core.AtomixBuilder;
import io.atomix.core.map.DistributedMap;
import io.atomix.core.profile.Profile;
import io.atomix.primitive.Replication;
import io.atomix.protocols.backup.MultiPrimaryProtocol;

public class AtomixClusterBootstrap {

	public static void main(String[] args) {

		AtomixBuilder builder = Atomix.builder().withMemberId("member1").withAddress("localhost:8700");

		builder.withMembershipProvider(BootstrapDiscoveryProvider.builder()
				.withNodes(Node.builder().withId("genesis-member").withAddress("localhost:8700").build(),
						Node.builder().withId("node1").withAddress("localhost:8701").build(),
						Node.builder().withId("node2").withAddress("localhost:8702").build())
				.build());

		builder.addProfile(Profile.dataGrid());

		Atomix atomix = builder.build();
		atomix.start().join();

		System.out.println("genesis-member joined the cluster");

		DistributedMap<Object, Object> map = atomix.mapBuilder("my-map").withProtocol(
				MultiPrimaryProtocol.builder("data").withBackups(2).withReplication(Replication.ASYNCHRONOUS).build())
				.build();

		TestSubject testSubject = new TestSubject("Leroux", "He Rocks!");

		map.put("foo", testSubject);

	}

}
